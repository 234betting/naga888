function setCookie(name, value, dayToLive = 1){
    const date = new Date();
    date.setTime(date.getTime() +  (dayToLive) * 60 * 60 * 1000);
    let expires = "expires=" + date.toUTCString();
    document.cookie = `${name}=${value}; ${expires}; path=/; SameSite=None; Secure`
}

function getCookie(name){
    const cDecoded = decodeURIComponent(document.cookie);
    const cArray = cDecoded.split("; ");
    let result = null;
    
    cArray.forEach(element => {
        if(element.indexOf(name) == 0){
            result = element.substring(name.length + 1)
        }
    })
    return result;
}

$(document).ready(function() {
    if(!getCookie('numberActive')){
        setTotalActiveCookies();
    } else {
        setHTMLTotalActive(getCookie('numberActive'));
    }
})

function totalActiveCookiesNumber() {
    const date = new Date();

    const todayDate = date.getDate();
    const todayMonth = date.getMonth() + 1;
    const todayYear = date.getFullYear();

    const numberActive = [];

    $('.button-buat-akun-container .the-button .information .number').each(function(index, numberElement) {
        numberActive.push(Math.floor(Math.random() * 1599) + 1);
    });

    numberActive.push(`${todayDate}/${todayMonth}/${todayYear}`);

    return numberActive;
}

function setHTMLTotalActive(totalActives) {
    const date = new Date();

    const todayDate = date.getDate();
    const todayMonth = date.getMonth() + 1;
    const todayYear = date.getFullYear();

    const actives = totalActives.split(',');

    if(actives[actives.length - 1] != `${todayDate}/${todayMonth}/${todayYear}`) {
        setTotalActiveCookies();
        return ;
    }

    const newTotalActives = [];

    $('.button-buat-akun-container .the-button .information .number').each(function(index, numberElement) {
        $(numberElement).text(actives[index]);

        const decisionMaker = Math.floor(Math.random() * 2);
        const totalPlus = Math.floor(Math.random() * 4)

        let newNumber;

        if(decisionMaker == 0) {
            newNumber = parseInt(actives[index])
        } else {
            newNumber = parseInt(actives[index]) + parseInt(totalPlus);
        }
        newTotalActives.push(newNumber);
    })

    newTotalActives.push(`${todayDate}/${todayMonth}/${todayYear}`);
    
    setCookie('numberActive', newTotalActives);
}

function setTotalActiveCookies() {
    const totalActiveCookies = totalActiveCookiesNumber();
    setHTMLTotalActive(totalActiveCookies.join(','));
    setCookie('numberActive', totalActiveCookies);
}